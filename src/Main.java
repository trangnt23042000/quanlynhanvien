
import controller.ManageController;
import controller.employee.ManageEmployeeController;
import controller.utils.ConnectionUtils;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import view.ManageView;
import view.employee.ListEmployeeView;
import view.employee.ManageEmployeeView;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Cuong Pham
 */
public class Main {

    public static void main(String[] args) {
        try {
            Connection conn = conn = ConnectionUtils.getMyConnection();
            ManageView manageView = new ManageView();
            ManageController manageController = new ManageController(manageView, conn);
            manageView.setVisible(true);
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
