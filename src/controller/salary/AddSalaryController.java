/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.salary;

import controller.dao.DAOSalaryGrade;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import javax.swing.JOptionPane;
import model.SalaryGrade;
import view.salary.AddSalaryView;
import view.salary.ManagerSalaryView;



/**
 *
 * @author Cuong Pham
 */
public class AddSalaryController {

    private DAOSalaryGrade dAOSalary;
    private Connection conn;
    private AddSalaryView addSalaryView;

    public AddSalaryController(Connection conn, AddSalaryView addSalaryView) {
        this.conn = conn;
        this.dAOSalary = new DAOSalaryGrade (conn);
        this.addSalaryView = addSalaryView;
        this.addSalaryView.addListener(new AddEmployeeListener());
    }

    class AddEmployeeListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == addSalaryView.getjButton1()) {
                SalaryGrade salary = new SalaryGrade();
                salary.setHighSalary(Float.parseFloat(addSalaryView.getjTextField2().getText()));
                salary.setLowSalary(Float.parseFloat(addSalaryView.getjTextField3().getText())); 
                dAOSalary.insert(salary);
                JOptionPane.showMessageDialog(addSalaryView, "Bạn đã thêm lương thành công!!!!!!!");
                
                addSalaryView.dispose();
                ManagerSalaryView managerSalaryView = new ManagerSalaryView();
                ManagerSalaryController managerSalaryController = new ManagerSalaryController(managerSalaryView, conn);
                managerSalaryView.setVisible(true);
                
            }
            
            if (e.getSource() == addSalaryView.getjButton2()) {
                addSalaryView.dispose();
                ManagerSalaryView managerSalaryView = new ManagerSalaryView();
                ManagerSalaryController managerSalaryController = new ManagerSalaryController(managerSalaryView, conn);
                managerSalaryView.setVisible(true);
            }

        }

    }

}
