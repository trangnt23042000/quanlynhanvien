/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.timekeeper;

import controller.ManageController;
import controller.dao.DAOEmployee;
import controller.employee.AddEmployeeController;
import controller.employee.ListEmployeeController;
import controller.employee.SearchEmployeeController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import model.Department;
import model.Employee;
import model.SalaryGrade;
import view.ManageView;
import view.employee.AddEmployeeView;
import view.employee.ListEmployeeView;
import view.employee.SearchEmployeeView;
import view.timekeeper.AddTimeKeeperView;
import view.timekeeper.ManageTimeKeeperView;

/**
 *
 * @author Cuong Pham
 */
public class ManageTimeKeeperController {
    private DAOEmployee dAOEmployee;
    private Connection conn;
    private ManageTimeKeeperView manageTimeKeeperView;

    public ManageTimeKeeperController(Connection conn, ManageTimeKeeperView manageTimeKeeperView) {
        dAOEmployee = new DAOEmployee(conn);
        this.conn = conn;
        this.manageTimeKeeperView = manageTimeKeeperView;
        this.manageTimeKeeperView.addListener(new ManageTimeKeeperListener());
    }
    
    class ManageTimeKeeperListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
          
            if (e.getSource() == manageTimeKeeperView.getjButton1()) {
                manageTimeKeeperView.dispose();
                List<Employee> employees = dAOEmployee.selectAll();
                AddTimeKeeperView addTimeKeeperView = new AddTimeKeeperView();

                addTimeKeeperView.getjComboBox1().removeAllItems();

                for (Employee employee : employees) {
                    addTimeKeeperView.getjComboBox1().addItem(employee.getDeptId() + "");
                }

//                AddEmployeeController addEmployeeController = new AddEmployeeController(conn, addTimeKeeperView);
                addTimeKeeperView.setVisible(true);

            }

//            if (e.getSource() == manageEmployeeView.getjButton3()) {
//                manageEmployeeView.dispose();
//                SearchEmployeeView searchEmployeeView = new SearchEmployeeView();
//                SearchEmployeeController searchEmployeeController = new SearchEmployeeController(true, conn, searchEmployeeView);
//                searchEmployeeView.setVisible(true);
//            }
//
//            if (e.getSource() == manageEmployeeView.getjButton2()) {
//                manageEmployeeView.dispose();
//                SearchEmployeeView searchEmployeeView = new SearchEmployeeView();
//                SearchEmployeeController searchEmployeeController = new SearchEmployeeController(false, conn, searchEmployeeView);
//                searchEmployeeView.setVisible(true);
//            }

            if (e.getSource() == manageTimeKeeperView.getjButton4()) {
                manageTimeKeeperView.dispose();
                ManageView manageView = new ManageView();
                ManageController manageController = new ManageController(manageView, conn);
                manageView.setVisible(true);
            }
        }

    }
}
