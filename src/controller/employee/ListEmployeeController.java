/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.employee;

import controller.ManageController;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import model.Employee;
import view.ManageView;
import view.employee.ListEmployeeView;
import view.employee.ManageEmployeeView;

/**
 *
 * @author Cuong Pham
 */
public class ListEmployeeController {

    private ListEmployeeView listEmployeeView;
    private Connection conn;

    public ListEmployeeController(ListEmployeeView listEmployeeView, Connection conn) {
        this.listEmployeeView = listEmployeeView;
        this.listEmployeeView.addListener(new ListEmployeeListener());
        this.conn = conn;
    }

    class ListEmployeeListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            listEmployeeView.dispose();
            ManageEmployeeView manageEmployeeView = new ManageEmployeeView();
            ManageEmployeeController manageEmployeeController = new ManageEmployeeController(manageEmployeeView, conn);
            manageEmployeeView.setVisible(true);

        }

    }

}
