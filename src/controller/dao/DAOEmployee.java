package controller.dao;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import model.Employee;

public class DAOEmployee extends IDAO<Employee> {

    public DAOEmployee(Connection conn) {
        this.conn = conn;
        try {
            this.statement = this.conn.createStatement();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Employee> selectAll() {
        List<Employee> result = new ArrayList<>();
        try {
            String sql = "SELECT * FROM EMPLOYEE";

            rs = statement.executeQuery(sql);
            while (rs.next()) {
                Employee e = new Employee(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDate(4),
                        rs.getBytes(5),
                        rs.getString(6),
                        rs.getFloat(7),
                        rs.getInt(8),
                        rs.getInt(9), rs.getInt(10));
                result.add(e);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

        return result;
    }

    @Override
    public List<Employee> selectByName(String name) {
        List<Employee> result = new ArrayList<>();
        try {
            String sql = "SELECT * FROM EMPLOYEE WHERE EMP_NAME LIKE '%" + name + "%'";

            rs = statement.executeQuery(sql);
            while (rs.next()) {
                Employee e = new Employee(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDate(4),
                        rs.getBytes(5),
                        rs.getString(6),
                        rs.getFloat(7),
                        rs.getInt(8),
                        rs.getInt(9), rs.getInt(10));
                result.add(e);
            }
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }

        return result;
    }

    @Override
    public int insert(Employee e) {
        String sql = "INSERT INTO EMPLOYEE ("
                + "EMP_NAME,"
                + "EMP_NO,"
                + "HIRE_DATE,"
                + "IMAGE,"
                + "JOB,"
                + "SALARY,"
                + "DEPT_ID,"
                + "MNG_ID,GRD_ID)"
                + "VALUES (?,?,?,?,?,?,?,?,?)";
        try {
            this.preStatement = this.conn.prepareStatement(sql);
            this.preStatement.setString(1, e.getEmpName());
            this.preStatement.setString(2, e.getEmpNo());
            this.preStatement.setDate(3, new java.sql.Date(e.getHireDate().getTime()));
            this.preStatement.setBytes(4, e.getImage());
            this.preStatement.setString(5, e.getJob());
            this.preStatement.setFloat(6, e.getSalary());
            this.preStatement.setInt(7, e.getDeptId());
            this.preStatement.setInt(8, e.getMngId());
            this.preStatement.setInt(9, e.getGrdId());
            int rowCount = this.preStatement.executeUpdate();
            return rowCount;
        } catch (SQLException e1) {
            e1.printStackTrace();
            return 0;

        }

    }

    @Override
    public int update(Employee e) {
        String sql = "UPDATE EMPLOYEE SET "
                + "EMP_NAME = ?,"
                + "EMP_NO = ?,"
                + "HIRE_DATE = ?,"
                + "IMAGE = ?,"
                + "JOB = ?,"
                + "SALARY = ?,"
                + "DEPT_ID = ?,"
                + "MNG_ID = ?,"
                + "GRD_ID = ? "
                + "WHERE EMP_ID = ?";
        try {
            this.preStatement = this.conn.prepareStatement(sql);
            this.preStatement.setString(1, e.getEmpName());
            this.preStatement.setString(2, e.getEmpNo());
            this.preStatement.setDate(3, new java.sql.Date(e.getHireDate().getTime()));
            this.preStatement.setBytes(4, e.getImage());
            this.preStatement.setString(5, e.getJob());
            this.preStatement.setFloat(6, e.getSalary());
            this.preStatement.setInt(7, e.getDeptId());
            this.preStatement.setInt(8, e.getMngId());
            this.preStatement.setInt(9, e.getGrdId());
            this.preStatement.setInt(10, e.getEmpId());

            int rowCount = this.preStatement.executeUpdate();

            return rowCount;
        } catch (SQLException e1) {
            e1.printStackTrace();
            return 0;

        }
    }

    @Override
    public void closeConnection() {
        try {
            this.conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int delete(int id) {
        try {
            String sql = "DELETE FROM EMPLOYEE WHERE EMP_ID = "+id;
            int rowCount = statement.executeUpdate(sql);
            return rowCount;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return 0;
        }
    }

}
