/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.SalaryGrade;

import controller.ManageController;
import controller.dao.DAODepartment;
import controller.dao.DAOEmployee;
import controller.dao.DAOSalaryGrade;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import model.SalaryGrade;
import view.ManageView;
import view.SalaryGrade.AddSalaryView;
import view.SalaryGrade.ListSalaryView;
import view.SalaryGrade.ManageSalaryView;


/**
 *
 * @author Cuong Pham
 */
public class ManageSalaryController {

    private DAOEmployee dAOEmployee;
    private DAODepartment dAODepartment;
    private DAOSalaryGrade dAOSalaryGrade;
    private ManageSalaryView manageSalaryView;
    private Connection conn;

    public ManageSalaryController(ManageSalaryView manageSalaryView, Connection conn) {
        this.manageSalaryView = manageSalaryView;
        this.manageSalaryView.addListener(new ManageSalaryListener());

        this.conn = conn;
        dAOEmployee = new DAOEmployee(conn);
        dAODepartment = new DAODepartment(conn);
        dAOSalaryGrade = new DAOSalaryGrade(conn);
    }

    class ManageSalaryListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == manageSalaryView.getjButton4()) {
                manageSalaryView.dispose();
                ListSalaryView listSalaryView = new ListSalaryView();
                DefaultTableModel model = (DefaultTableModel) listSalaryView.getjTable1().getModel();
                model.setRowCount(0);
                List<SalaryGrade> list = dAOSalaryGrade.selectAll();
                for (SalaryGrade salary : list) {
                    model.addRow(salary.toObject());
                }
                ListSalaryController listSalaryController = new ListSalaryController(listSalaryView, conn);
                listSalaryView.setVisible(true);
            }

            if (e.getSource() == manageSalaryView.getjButton1()) {
                manageSalaryView.dispose();
                AddSalaryView addSalaryView = new AddSalaryView();

                addSalaryView.getjTextField2().setText("");
                addSalaryView.getjTextField3().setText("");

                AddSalaryController addSalaryController = new AddSalaryController(conn, addSalaryView);
                addSalaryView.setVisible(true);

            }


            if (e.getSource() == manageSalaryView.getjButton5()) {
                manageSalaryView.dispose();
                ManageView manageView = new ManageView();
                ManageController manageController = new ManageController(manageView, conn);
                manageView.setVisible(true);
            }

        }

    }

}
