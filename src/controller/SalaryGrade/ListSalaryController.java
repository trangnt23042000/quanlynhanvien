/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.SalaryGrade;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import view.SalaryGrade.ListSalaryView;
import view.SalaryGrade.ManageSalaryView;

/**
 *
 * @author Cuong Pham
 */
public class ListSalaryController {

    private ListSalaryView listSalaryView;
    private Connection conn;

    public ListSalaryController(ListSalaryView listSalaryView, Connection conn) {
        this.listSalaryView = listSalaryView;
        this.listSalaryView.addListener(new ListSalaryListener());
        this.conn = conn;
    }

    class ListSalaryListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            listSalaryView.dispose();
            ManageSalaryView manageSalaryView = new ManageSalaryView();
            ManageSalaryController manageSalaryController = new ManageSalaryController(manageSalaryView, conn);
            manageSalaryView.setVisible(true);

        }

    }

}
